import React from 'react';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <p>
          Bonjour Hassan
        </p>
        <h1>Welcome to My Educational Site!</h1>
      </header>
    </div>
  );
}

export default App;
